module InferTypes
  #
  # This class is used to represent Arrays as Symbolic Expressions (SymExprs)
  #
  # @example: Any array can be turned into a SymExpr. We print symbols without
  # the colon since SymExprs make heavy use of symbols.
  #
  #   [:a, 1, [2, 3]].to_symexpr #=> (a 1 (2 3))
  #
  class SymExpr < Array
    # Deep copy the array, changing sub-arrays into SymExprs
    #
    # @param [Array] entries
    def initialize(entries)
      entries.each do |entry|
        if entry.is_a? Array
          self << SymExpr.new(entry)
        else
          self << entry
        end
      end
    end

    # Show the array using parens instead of brackets, and removing
    # the colon in the symbol. This makes the SymExpr look a lot like
    # an s-expr, ala Lisp.
    #
    # @return [String]
    def inspect
      %Q{(#{self.map {|e| e.is_a?(Symbol) ? e.to_s : e.inspect }.join(' ')})}
    end

    alias :to_s :inspect

    # @return [Boolean]
    def boxed?
      first.is_a? SymExpr
    end

    # @return [SymExpr]
    def box
      if boxed?
        self
      else
        SymExpr.new([self])
      end
    end

    # Change a SymExpr back into an array
    #
    # @return [Array]
    def to_a
      a = []
      self.each do |entry|
        if entry.is_a? SymExpr
          a << entry.to_a
        else
          a << entry
        end
      end
      a
    end
  end
end
