require 'lib/infer_types/types/concrete'
require 'lib/infer_types/types/abstract'
require 'lib/infer_types/types/optional'
require 'lib/infer_types/types/block'
require 'lib/infer_types/types/union'
require 'lib/infer_types/types/parametric'

require 'treetop'

module InferTypes
  class Method
    # Method::Signature : A method signature is what we generate
    # when we are finished inferring the type for a method.
    #
    # We also generate method signatures based on the parser for
    # DRuby syntax, in which case we attempt to match the user
    # given type with the one inferred
    #
    class Signature
      attr_accessor :name, :args, :return_type, :block, :parameters

      def self.from_hash(hash)
        new(hash[:name], (hash[:args] || []), hash[:return_type], hash[:block], hash[:params])
      end

      def initialize(name, args, return_type, block = nil, params = nil)
        # TODO : Args must be of type Arguments
        raise "Args must be an array!" unless args.is_a? Array
        @name, @args, @return_type, @block, @params = name.to_s, args, return_type, block, params
      end

      def matches_args?(args)
        # TODO
        args == @args
      end

      def method_name
        if @name =~ /[A-Za-z0-9]/
          @name
        else
          @name.inspect
        end
      end

      module Helper
        def self.extended(base)
          base.send :attr_accessor, :method_signatures
        end

        def parse_type_signature(text)
          @method_signatures ||= []
          @method_signatures << perform_type_signature_parse(text)
        end

        def perform_type_signature_parse(text)
          Treetop.load 'lib/infer_types/method/signature/parser'
          parser = MethodSignatureParser.new
          result = parser.parse(text)

          if result.nil?
            raise ({:failure => parser.failure_reason, :text => text}.inspect)
          else
            result.to_sig
          end
        end
      end
    end
  end
end

Class.send :include,  InferTypes::Method::Signature::Helper
Module.send :include, InferTypes::Method::Signature::Helper
