module InferTypes
  class Method
    class Binding
      attr_accessor :key, :expr

      def initialize(params = {})
        @key, @expr = params[:key], params[:expr]
      end

      def inspect
        "#{@key} := #{expr.inspect}"
      end

      def ==(other)
        (other.key == self.key) and (other.expr == self.expr)
      end

      alias :to_s :inspect
    end

    # Store the variable & expression bindings for this method
    #
    # @example:
    #   puts m.bindings.inspect =>
    #
    #   1 := (lasgn s (lit 500))
    #   2 := (lit 500)
    #   3 := (lasgn t (lvar s))
    #   4 := (lvar s)
    #   5 := (lvar t)
    #
    class Bindings < Array
      def initialize(env)
        @env   = env
        @total = 0
        @exprs = {}

        env.method_body.each do |stmt|
          register_expr(stmt)
        end

        @exprs.invert.sort.map do |key, expr|
          self << Binding.new(:key => key, :expr => expr)
        end
      end

      def [](expr)
        @exprs[expr]
      end

      def inspect
        map(&:inspect).join("\n")
      end

      def register(e)
        unless e.empty?
          if @exprs[e].nil?
            @total += 1
            @exprs[e] = @total
          end
        end
      end

      def register_expr(expr)
        register(expr)
        Patterns.match_any(expr).each do |key, value|
          if value.is_a? SymExpr
            if value.boxed?
              value.each {|v| register_expr(v) }
            else
              register_expr(value)
            end
          end
        end
      end
    end
  end
end
