module InferTypes
  class Method
    class Equations < Array
      class ReturnType
        def self.[](name)
          new(name)
        end

        def initialize(name)
          @name = name
        end

        def inspect
          "<return-type-of #{@name.inspect}>"
        end

        def lookup(self_type)
          # @todo (farleyknight@gmail.com): Find the appropriate method using the
          # self-type, which includes the current class and the current included
          # modules.
          self_type.included_modules.each do |mod|
            # @note (farleyknight@gmail.com): This is required because of some
            # 'clean-slate' modules found in rspec.
            if mod.respond_to? :lookup_method
              unless (m = mod.lookup_method(@name)).nil?
                return m.return_type
              end
            end
          end

          # After looking through all the included modules of this self, we look
          # up the method in the class of self.
          m = self_type.klass.lookup_method(@name)
          return m.return_type unless m.nil?
          m = self_type.superclass.lookup_method(@name)
          return m.return_type unless m.nil?

          raise "Couldn't find match for #{@name}. How do we handle this case? "
        end
      end
    end
  end
end



