module InferTypes
  class Method
    class Equations < Array
      class Solver
        attr_accessor :id, :solutions, :equations

        def initialize(id, equations, method)
          @id        = id
          @solutions = []
          @equations = equations
          @method    = method

          solve!([@id])
        end

        # Use a back tracking search algorithm to find all the possible end-points
        # for a particular expression id, provided by `id`
        #
        # @todo (farleyknight@gmail.com): Figure out if there is a way to make this
        # kind of thing **fast**. Probably look into using Ruby extensions for
        # building trees and traversing them.
        #
        # @param [Fixnum] id
        # @return [Array<Class, ??target??>]
        def solve!(ids)
          until ids.empty?
            new_ids = []
            until ids.empty?
              i = ids.pop
              @equations.each do |equation|
                find_solution(i, new_ids, equation)
              end
            end

            ids = new_ids.dup
          end
        end

        # @todo (farleyknight@gmail.com) Show in some documentation what the
        # the values in the loop look like as you iterate through it.
        def find_solution(i, new_ids, eq)
          if eq.source == i
            eq.targets.each do |t|
              if t.is_a?(Class)
                @solutions << t
              elsif t.is_a?(Symbol)
                # If no equations match t
                if no_equations_matching? t
                  # then we have a method call from an outside source
                  #
                  # @example:
                  #
                  #   def m
                  #     puts 'whatever'
                  #   end
                  #
                  # Would mean that `puts` is in some included module,
                  # in this case, in Kernel.
                  #
                  type = Equations::ReturnType[t].lookup(@method.self_type)
                  if type.is_a? Array
                    @solutions += type
                  else
                    @solutions << type
                  end
                else
                  new_ids << t
                end
              else
                new_ids   << t
              end
            end
          end
        end

        # We are looking for equations of the form
        #
        # x => y
        #
        # that match x == type
        #
        # Where y is the target value and x is the
        # source value.
        #
        # @todo: @example
        #
        def no_equations_matching?(type)
          @equations.detect {|eq| eq.source == type }.nil?
        end
      end
    end
  end
end

