module InferTypes
  # Type Inferencing Algorithm:
  #
  # Step 1:
  #
  # The first step is to go through and tag everything
  # The expr stack is nothing more than a list of
  # the exprs we've found and the id-tags we've given
  # them.
  #
  # Step 2:
  #
  # The second step is to build a stack of equations.
  #
  # In this step, we replace subexpressions with
  # the tags for our current expressions.
  #
  class Method
    class Environment
      # @attribute [rw] [Method]
      attr_accessor :parent

      # @param [??] method_body
      # @param [Class] klass
      # @param [String, Symbol] method_name
      # @param [Method] parent
      def initialize(method_body, klass, method_name, parent)
        @method_info = match_defn(method_body)
        @klass       = klass
        @method_name = method_name
        @parent      = parent
      end

      # @return [??]
      def match_defn(body)
        InferTypes::Patterns::Defn.match(body)
      end

      # @return [??]
      def method_body
        @method_body ||= @method_info["body"].box
      end

      # @return [Bindings]
      def bindings
        @bindings ||= Bindings.new(self)
      end

      # @return [Equations]
      def equations
        @equations ||= Equations.new(self)
      end
    end
  end
end
