require 'lib/infer_types/method/equations/return_type'
require 'lib/infer_types/method/equations/solver'

module InferTypes
  class Method
    # An equation object represents a binding of a source and a target. For example, lets say we have
    # the source code:
    #
    # a = [1,2,3]
    #
    # Where `a` has expression id #4. Then we might have:
    #
    # #4 => Array<Fixnum>
    #
    class Equation
      attr_accessor :source, :target

      # @param [Hash] params
      # @option [??] params :source
      # @option [??] params :target
      def initialize(params = {})
        @source = params[:source]
        @target = params[:target]
      end

      # @return [String]
      def inspect
        "#{@source} => #{@target}"
      end

      # @return [Array<??>]
      def targets
        Array(@target)
      end

      alias :to_s :inspect

      # @return [Boolean]
      def ==(other)
        self.source == other.source and self.target == other.target
      end
    end

    class Equations < Array
      # @param [Method::Environment]
      def initialize(env)
        @env       = env
        (@bindings = env.bindings).each do |b|
          expr = b.expr
          id   = b.key
          # @todo (farleyknight@gmail.com): This is _horrible_ performance-wise!
          # We only need to check the patterns that match `expr` by using the first
          # symbol from expr! We should not have to iterate through every single one
          # pattern and check!
          Patterns::RubyPattern.all_patterns.each do |p|
            if p.match? expr
              m  = p.match(expr)
              es = p.new(env).equations(id, m)
              raise "Couldn't find equation for expression! #{expr}" if m.nil?
              es.each {|e| self << e }
            end
          end
        end
      end

      # Find all possible solutions for this id
      def solve_for(id)
        Solver.new(id, self, @env.parent).solutions
      end

      def inspect
        map(&:inspect).join("\n")
      end
    end
  end
end
