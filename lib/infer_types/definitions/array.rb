
##% Array<t>
class Array ### DONE
  include Enumerable
  ##% "&"<u> : Array<u> -> Array<t> # XXX: should I constrain u <= t?
  def &(p0) end
  ##% "*" : Integer -> Array<t>
  ##% "*" : String -> String
  def *(p0) end
  ##% "+"<u> : [to_ary:() -> Array<u>] -> Array<t or u>
  def +(p0) end
  ##% "-"<u> : Array<u> -> Array<t> # still t!
  def -(p0) end
  ##% "<<" <u> : u -> Array<t or u>
  def <<(p0) end
  ##% "<=>" <u> : Array<u> -> Fixnum
  def <=>(p0) end
  ##% "=="<v> : v -> Boolean
  def ==(p0) end
  ##% "[]" : Range -> Array<t>
  ##% "[]" : (Fixnum, Fixnum) -> Array<t> # DAVIDAN
  ##% "[]" : Fixnum -> t # DAVIDAN
  def [](p0, *rest) end
  ##% "[]=" <u> ; u <= t: (Fixnum, u) -> u
  ##% "[]=" <u> ; u <= t: (Fixnum, Fixnum, u) -> u
  ##% "[]=" <u> ; u <= t: (Fixnum, Fixnum, Array<u>) -> Array<u>
  ##% "[]=" <u> ; u <= t: (Range, u) -> u
  ##% "[]=" <u> ; u <= t: (Range, Array<u>) -> Array<u>
  def []=(p0, *rest) end
  ##% assoc<self,u> ; self <= Array<Array<u>> : u -> Array<u>
  def assoc(p0) nil end
  ##% at : Fixnum -> t
  def at(p0) end
  ##% clear : () -> Array<t> # XXX: or is it just Array?
  def clear() end
  ##% collect<u> : () {t -> u} -> Array<u>
  def collect() end
  ##% collect!<u> ; u <= t : () {t -> u} -> Array<u>
  def collect!() end
  ##% compact : () -> Array<t>
  def compact() end
  ##% compact! : () -> Array<t>
  def compact!() end
  ##% concat : Array<t> -> Array<t>
  def concat(p0) end
  ##% delete : t -> t
  ##% delete<u> : (t) {() -> u} -> t or u
  def delete(p0) end
  ##% delete_at : Fixnum -> t
  def delete_at(p0) end
  ##% delete_if<any> : () {t -> any} -> Array<t>
  def delete_if() end
  ##% each<u> : () {t -> u} -> Array<t>
  def each() end
  ##% each_index<u> : () {Fixnum -> u} -> Array<t>
  def each_index() end
  ##% empty? : () -> Boolean
  def empty?() end
  ##% eql?<v> : v -> Boolean #Array<t> -> Boolean
  def eql?(p0) end
  ##% fetch : Fixnum -> t
  ##% fetch<u> : (Fixnum, u) -> t or u
  ##% fetch<u> : (Fixnum) {Fixnum -> u} -> u
  def fetch(p0, *rest) end
  ##% fill<u> ; u <= t : (u, ?Fixnum, ?Fixnum) -> Array<t>
  ##% fill<u> ; u <= t : (u, Range) -> Array<t>
  ##% fill<u> ; u <= t : () {Fixnum -> u} -> Array<u> # notice Array<u>
  ##% fill<u> ; u <= t : (Fixnum, ?Fixnum) {Fixnum -> u} -> Array<t>
  ##% fill<u> ; u <= t : (Range) {Fixnum -> u} -> Array<t>
  def fill(*rest) end
  ##% first : () -> t
  ##% first : Fixnum -> Array<t>
  def first(*n) end
  ##% flatten : () -> Array # no error but not precise
  def flatten() end  # can't give this function a real type!
  ##% flatten! : () -> Array # XXX: throws type error since t is invalid
  def flatten!() end
  ##% frozen? : () -> Boolean
  def frozen?() end
  ##% hash : () -> Fixnum
  def hash() end
  ##% include? : t -> Boolean
  def include?(p0) end
  ##% index : t -> Fixnum
  def index(p0) end
  ##% indexes : (*Fixnum) -> Array<t>
  def indexes(*rest) end
  ##% indices : (*Fixnum) -> Array<t>
  def indices(*rest) end
  ##% initialize : (?Fixnum, ?t) -> []
  ##% initialize : Array<t> -> []
  ##% initialize : (Fixnum) {Fixnum -> t} -> []
  def initialize(*rest) end
  ##% insert<u> ; u <= t : (Fixnum, *u) -> Array<t>
  def insert(ind, *rest) end
  ##% inspect : () -> String
  def inspect() end
  ##% join : (?String) -> String
  def join(*rest) end
  ##% last : () -> t
  ##% last : Fixnum -> Array<t>
  def last(*n) end
  ##% length : () -> Fixnum
  def length() end
  ##% map<u> : () {t -> u} -> Array<u>
  def map() end
  ##% map!<u> ; u <= t : () {t -> u} -> Array<u>
  def map!() end
  ##% nitems : () -> Fixnum
  def nitems() end
  ##% pack : String -> String
  def pack(p0) end
  ##% pop : () -> t
  def pop() end
  ##% push<u> ; u <= t : (*u) -> Array<t>
  def push(*rest) end
  ##% rassoc<u> : u -> t # ; t <= Array<u>
  def rassoc(p0) end
  ##% reject<u>: () {t -> u} -> Array<t>
  def reject() end
  ##% reject!<u> : () {t -> u} -> Array<t>
  def reject!() end
  ##% replace<u> ; u <= t : Array<u> -> Array<u>
  def replace(p0) end
  ##% reverse : () -> Array<t>
  def reverse() end
  ##% reverse! : () -> Array<t>
  def reverse!() end
  ##% reverse_each<u> : () {t -> u} -> Array<t>
  def reverse_each() end
  ##% rindex : t -> Fixnum
  def rindex(p0) end
  ##% select<u> : () {t -> u} -> Array<t>
  def select() end
  ##% shift : () -> t
  def shift() end
  ##% size : () -> Fixnum
  def size() end
  ##% slice : Fixnum -> t
  ##% slice : (Fixnum, Fixnum) -> Array<t>
  ##% slice : Range -> Array<t>
  def slice(*rest) end
  ##% slice! : Fixnum -> t
  ##% slice! : (Fixnum, Fixnum) -> Array<t>
  ##% slice! : Range -> Array<t>
  def slice!(p0, *rest) end
  # #% sort<self> ; self <= [each<u>: () {["<=>" : t -> Fixnum] -> u} -> Array<t>] \
  # #% : () -> Array<t>
  # #% sort : () {(t, t) -> Fixnum} -> Array<t>
  # def sort() end
  # #% sort!<self> ; self <= [each<u>: () {["<=>" : t -> Fixnum] -> u} -> Array<t>] \
  # #% : () -> Array<t>
  # #% sort! : () -> Array<t>
  ##% sort! : () {(t, t) -> Fixnum} -> Array<t>
  def sort!() end
  ##% to_a : () -> Array<t>
  def to_a() end
  ##% to_ary : () -> Array<t>
  def to_ary() end
  ##% to_s : () -> String
  def to_s() end
  ##% transpose : () -> Array<t>
  def transpose() end
  ##% uniq : () -> Array<t>
  def uniq() end
  ##% uniq! : () -> Array<t>
  def uniq!() end
  ##% unshift<u> ; u <= t : (*u) -> Array<t>
  def unshift(*rest) end
  ##% values_at : (*(Fixnum or Range)) -> Array<t>
  def values_at(*rest) end
  ##% zip<u> : (*Array<u>) -> Array<Array<u or t> >
  def zip(*rest) end
  ##% "|"<u> ; u <= t : Array<u> -> Array<t>
  def |(p0) end
  ##% Array."[]" : (*t) -> Array<t>
  def Array.[](*rest) end
end
