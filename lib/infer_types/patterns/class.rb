
module InferTypes::Patterns
  class Class < RubyPattern
    # @see RubyPattern#patterns
    def pattern
      [:class, var("class"), [:const, var("superclass")], var("body")]
    end

    # @see RubyPattern#equations
    def equations(expr_id, match)
      [
        # @note: 1) The class definition itself always returns nil.
        # @example: irb> class A; end #=> nil
        eqn(expr_id, nil),

        # @note: 2) The class name is always an instance of Class
        eqn(match["class"], Class)
      ]
    end
  end
end

