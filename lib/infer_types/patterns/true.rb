module InferTypes::Patterns
  class True < RubyPattern
    # @see RubyPattern#pattern
    # @todo: Example
    def pattern
      [:true]
    end

    # @see RubyPattern#equations
    # @todo: Example
    def equations(expr_id, match)
      [eqn(expr_id, TrueClass)]
    end
  end
end
