
module InferTypes::Patterns
  class Yield < RubyPattern
    # @see RubyPattern#pattern
    def pattern
      [:yield, var("args")]
    end

    # @see RubyPattern#equations
    def equations(expr, match)
      # ??
    end
  end
end

