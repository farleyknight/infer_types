module InferTypes::Patterns
  class Fcall < RubyPattern
    # @see RubyPattern#pattern
    def pattern
      [:fcall, var("name"), [:array, var("args")]]
    end

    # @see RubyPattern#equations
    def equations(expr_id, match)
      [
        # 1) The expression itself is the result of the call to 'name'
        eqn(expr_id, match["name"]),
        # 2) Self object must respond to the method 'name'
        eqn(self_type, respond_to(match["name"], id_of(match["args"])))
        # @todo: 3) The args of this call must match the args of the method 'name'
      ]
    end
  end
end
