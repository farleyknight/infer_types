module InferTypes::Patterns
  class Call < RubyPattern
    # @see RubyPattern#pattern
    # @todo: Example
    def pattern
      [:call, var("obj"), var("name"), [:array, var("args")]]
    end

    # @see RubyPattern#equations
    # @todo: Example
    def equations(expr_id, match)
      [eqn(expr_id, match["name"])]
    end
  end
end
