module InferTypes::Patterns
  class RubyPattern
    include InferTypes::Patterns::Matching

    # @attribute [Method::Environment]
    attr_accessor :env

    # @param [Method::Environment] env
    def initialize(env)
      @env    = env
      @method = env.parent
    end

    # @return [Array<RubyPattern>]
    def self.all_patterns
      @@all_patterns || []
    end

    def self.inherited(klass)
      @@all_patterns ||= []
      @@all_patterns << klass
    end

    # @param [Array] expr
    # @return [Pattern]
    def self.match(expr)
      Pattern.new(allocate.pattern).match(expr)
    end

    # @todo Do we always want to fail without handling the error somehow?
    #
    # @param [SymExpr] expr
    # @return [Boolean]
    def self.match?(expr)
      begin
        match(expr)
      rescue
        return false
      end
    end

    # @see RubyPattern#equations
    def self.equations(expr_id, match)
      self.new.equations(expr_id, match)
    end

    # The pattern for this RubyPattern, which is used in RubyPattern.match
    #
    # @return [Array]
    def pattern
      raise NotImplementedError
    end

    # Takes the expression id (expr_id) and produces a binding for it
    # along with the match (match) from the pattern.
    #
    # @param [Fixnum] expr_id
    # @param [Hash] match
    # @return [Array<Equation>]
    def equations(expr_id, match)
      raise NotImplementedError
    end

    # @return [SelfType]
    def self_type
      @method.self_type
    end

    # @return [RespondTo]
    def respond_to(name, args)
      InferTypes::Types::RespondTo.new(:name => name, :args => args)
    end

    # @param [SymExpr] match
    # @return [Tag, Class]
    def id_of(match)
      env.bindings[match]
    end

    # @param [??] source
    # @param [??] target
    # @return [Method::Equation]
    def eqn(source, target)
      InferTypes::Method::Equation.new(:source => source, :target => target)
    end
  end
end
