
module InferTypes::Patterns
  class Str < RubyPattern
    # @see RubyPattern#pattern
    def pattern
      [:str, var("val")]
    end

    # @see RubyPattern#equations
    def equations(expr_id, match)
      [eqn(expr_id, String)]
    end
  end
end
