
module InferTypes
  module Patterns
    class Lvar < RubyPattern
      # @see RubyPattern#pattern
      def pattern
        [:lvar, var("name")]
      end

      # @see RubyPattern#equations
      def equations(expr_id, match)
        [eqn(expr_id, match["name"])]
      end
    end
  end
end
