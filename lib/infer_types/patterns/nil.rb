
module InferTypes::Patterns
  class Nil < RubyPattern
    # @see RubyPattern#pattern
    # @todo: Example
    def pattern
      [:nil]
    end

    # @see RubyPattern#equations
    # @todo: Example
    def equations(expr_id, match)
      [eqn(expr_id, NilClass)]
    end
  end
end

