module InferTypes::Patterns
  class Lit < RubyPattern
    # @see RubyPattern#pattern
    def pattern
      [:lit, var("val")]
    end

    # @see RubyPattern#equations
    def equations(expr_id, match)
      # The type of (lit val) for some literal `val` is always the class of `val`
      [eqn(expr_id, match["val"].class)]
    end
  end
end

