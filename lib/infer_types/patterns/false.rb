
module InferTypes::Patterns
  class False < RubyPattern
    # @see RubyPattern#pattern
    def pattern
      [:false]
    end

    # @see RubyPattern#equations
    def equations(expr_id, match)
      [eqn(expr_id, FalseClass)]
    end
  end
end

