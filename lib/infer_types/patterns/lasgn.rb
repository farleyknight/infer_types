module InferTypes
  module Patterns
    class Lasgn < RubyPattern
      # @see RubyPattern#pattern
      def pattern
        [:lasgn, var("name"), var("expr")]
      end

      # @see RubyPattern#equations
      def equations(expr_id, match)
        [
          # @note: 1) The type of (lasgn name expr) for some name `name` is always the type of `expr`
          Method::Equation.new(:source => expr_id,       :target => env.bindings[match["expr"]]),
          # @note: 2) The type of `name` is always the type of `expr`
          Method::Equation.new(:source => match["name"], :target => env.bindings[match["expr"]])
        ]
      end
    end
  end
end
