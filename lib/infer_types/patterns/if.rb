
module InferTypes::Patterns
  class If < RubyPattern
    # @see RubyPattern#pattern
    def pattern
      [:if, var("pred"), var("true"), var("false")]
    end

    # @see RubyPattern#equations
    def equations(expr_id, match)
      [
        eqn(expr_id, id_of(match["true"])),
        eqn(expr_id, id_of(match["false"]))
      ]
    end
  end
end

