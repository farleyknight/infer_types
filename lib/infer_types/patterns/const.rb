
module InferTypes
  module Patterns
    class Const < RubyPattern
      # @see RubyPattern#patterns
      def pattern
        [:const, var("name")]
      end

      # @see RubyPattern#equations
      def equations(expr_id, match)
        name = Object.const_get(match["name"])
        [Equation.new(:source => expr_id, :target => name)]
      end
    end
  end
end
