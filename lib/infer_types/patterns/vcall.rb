module InferTypes::Patterns
  class Vcall < RubyPattern
    # @see RubyPattern#pattern
    def pattern
      [:vcall, var("name")]
    end

    # @see RubyPattern#equations
    def equations(expr_id, match)
      [eqn(expr_id, match["name"])]
    end
  end
end
