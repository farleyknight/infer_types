
module InferTypes
  module Patterns
    class Defn < RubyPattern
      # @see RubyPattern#pattern
      def pattern
        [:defn, var("name"), [:scope, [:block, [:args, var("args")], var("body")]]]
      end

      # @see RubyPattern#equations
      def equations(expr_id, match)
        # ??
      end
    end
  end
end
