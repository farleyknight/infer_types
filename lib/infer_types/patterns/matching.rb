module InferTypes
  #
  # Question: Does pattern matching work on Arrays,
  # not just SymExps?
  #
  # Answer: It is available if and only if the user
  # specifically asks for it.
  #
  # Otherwise, it's something they will have to
  # 'include' themselves
  #

  module Patterns
    module Matching
      class Var
        attr_accessor :name, :opts
        def initialize(name, opts = {})
          @name = name
          @opts = opts
        end

        def inspect
          %Q{var:#{@name}}
        end
      end

      def var(name)
        Var.new(name)
      end

      class Pattern < Array
        def initialize(array = [])
          raise "Constructor must be passed an array!" unless array.is_a? Array
          array.each do |elem|
            if elem.is_a? Array
              self << Pattern.new(elem)
            else
              self << elem
            end
          end
        end

        def inspect
          %Q{Pattern: #{self.to_a.inspect}}
        end

        # match? - Determine if we have a match
        def match?(other)
          begin
            match(other)
            return true
          rescue
            return false
          end
        end

        def extract(other, name)
          if match? other
            match(other)[name]
          else
            nil
          end
        end

        # Perform a match
        #
        # Attempt to unionize two aribtrarily tested arrays, e.g. trees, with meta-
        # variables only available to our own pattern matching objects.
        #
        def match(other, matches = {})
          if other.nil? or other.empty?
            raise "Cannot match #{other.inspect} against pattern #{self.inspect}!"
          elsif self.empty?
            raise "Cannot match #{other.inspect} against pattern #{self.inspect}!"
          end

          self.zip_xs(other) do |child_other, pattern|
            if pattern.is_a? Var
              matches[pattern.name] = child_other
            elsif child_other.is_a? Array
              pattern.match(child_other, matches)
            elsif pattern != child_other
              raise [
                "Could not match #{other} - #{child_other} against #{pattern}!",
                "Original: #{other.inspect}, Pattern: #{self.inspect}!"
              ].join("\n")
            elsif pattern == child_other
              # Nothing!
            else
              raise "Out of options for other: #{child_other.inspect} and #{pattern.inspect}!"
            end
          end

          matches
        end

        # zip_xs - Slight modification of original zip
        # If we are at the end of the list, we want to
        # cut the rest off and hand it back.
        #
        # Ex:
        #
        # [1,2,3,4,5].zip_xs([1,2,4]) {|a,b| puts [a,b].inspect }
        # [1, 1]
        # [2, 2]
        # [[3, 4, 5], 4]
        #
        # The last line is different from 'classic' zip
        #
        # Ex:
        #
        # [1,2,3,4,5].zip([1,2,4]) {|a,b| puts [a,b].inspect }
        # [1, 1]
        # [2, 2]
        # [3, 4]
        # [4, nil]
        # [5, nil]
        #
        def zip_xs(other, &block)
          (0..other.length-1).each do |i|
            if self.length == i or self.length < i
              break
            elsif self.length == i+1 and other.length > i+1
              block.call(other[i..-1], self[i]) and break
            else
              block.call(other[i], self[i])
            end
          end
        end
      end
    end
  end
end
