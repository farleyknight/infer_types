require 'lib/infer_types/patterns/matching'

require 'lib/infer_types/patterns/ruby_pattern'

require 'lib/infer_types/patterns/class'
require 'lib/infer_types/patterns/const'
require 'lib/infer_types/patterns/defn'

require 'lib/infer_types/patterns/if'

require 'lib/infer_types/patterns/lvar'
require 'lib/infer_types/patterns/lasgn'

require 'lib/infer_types/patterns/str'
require 'lib/infer_types/patterns/false'
require 'lib/infer_types/patterns/true'
require 'lib/infer_types/patterns/nil'
require 'lib/infer_types/patterns/lit'

require 'lib/infer_types/patterns/call'
require 'lib/infer_types/patterns/fcall'
require 'lib/infer_types/patterns/vcall'
require 'lib/infer_types/patterns/yield'

require 'lib/infer_types/method/equations'

String.class_eval do
  def capitalize
    self[0..0].upcase + self[1..-1]
  end
end

module InferTypes
  module Patterns
    extend Matching

    def self.[](other)
      const_get(other.to_s.capitalize.to_sym)
    end

    def self.match_any(expr)
      RubyPattern.all_patterns.each do |pattern|
        if (m = pattern.match?(expr))
          return m
        end
      end
    end
  end
end
