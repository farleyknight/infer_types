
class Array
  def self.wrap(object)
    if object.nil?
      []
    elsif object.respond_to?(:to_ary)
      object.to_ary || [object]
    else
      [object]
    end
  end

  def box
    if first.is_a? Array
      self
    else
      Array.new([self])
    end
  end

  def to_symexpr
    InferTypes::SymExpr.new(self)
  end
end
