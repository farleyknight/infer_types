require 'lib/infer_types/method'
require 'lib/infer_types/types/self_type'
require 'lib/infer_types/types/respond_to'
require 'lib/infer_types/definitions'

module InferTypes
  module Types
    def self.schemas
      self.constants.map {|c| self.const_get(c) }
    end
  end
end

