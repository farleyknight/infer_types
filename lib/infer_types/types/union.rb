module InferTypes
  module Types
    # t1 or t2
    # String or Array
    class Union
      attr_accessor :types

      def initialize(types)
        raise "Types must be an array!" unless types.is_a? Array
        @types = types.uniq
      end

      def inspect
        @types.map {|t| t.inspect }.join(' or ') + " (Union)"
      end

      def signature_inspect
        @types.map {|t| t.signature_inspect }.join(' or ')
      end

      alias :to_s :inspect
    end
  end
end
