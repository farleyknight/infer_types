module InferTypes::Types
  class RespondTo
    attr_accessor :name, :args
    def initialize(params = {})
      @name = params[:name]
      @args = Array(params[:args])
    end

    def inspect
      "<##{@name}(#{@args.map(&:to_s).join(',')})>"
    end

    def ==(other)
      @name == other.name and @args == other.args
    end

    alias :to_s :inspect
  end
end
