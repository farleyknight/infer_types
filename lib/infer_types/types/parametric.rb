module InferTypes
  module Types
    # Array<t>
    # Array<String>
    class Parametric
      @@parametric_types = {}

      attr_accessor :klass, :params

      def self.from_class_and_params(klass, params)
        unless params.is_a? Array
          raise "Types must be an array!"
        end

        unless params.all? {|p| Types.schemas.include? p.class }
          raise "Types must be an array of types! Instead it was #{params.inspect}"
        end

        parametric = self.allocate
        parametric.klass  = klass
        parametric.params = params
        @@parametric_types[[klass, params]] ||= parametric
      end

      def self.new(*args)
        raise "Cannot use Parametric.new!"
      end

      def inspect
        ps = "<" + @params.map {|p| p.inspect }.join(',') + ">"
        %Q{#{@klass.name}#{ps} (Parametric)}
      end

      def signature_inspect
        @klass.name + "<" + @params.map {|p| p.signature_inspect }.join(',') + ">"
      end

      alias :to_s :inspect
    end
  end
end
