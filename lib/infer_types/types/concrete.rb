module InferTypes
  module Types
    class Concrete
      attr_accessor :klass

      @@concrete_types = {}

      def self.from_class(klass)
        if klass.is_a? InferTypes::Types::Concrete
          from_class(klass.klass)
        elsif klass.is_a? InferTypes::Types::Abstract
          klass
        elsif klass.is_a? Class or klass.is_a? InferTypes::Types::Parametric
          concrete = self.allocate
          concrete.klass = klass
          @@concrete_types[klass] ||= concrete
        else
          raise "klass must be a class or a parametric! Got #{klass.class}"
        end
      end

      def self.new(*args)
        raise "Cannot use Concrete.new!"
      end

      def inspect
        "#{@klass.inspect} (Concrete)"
      end

      def signature_inspect
        @klass.signature_inspect
      end

      def method_signatures
        @klass.method_signatures
      end

      alias :to_s :inspect
    end
  end
end
