
module InferTypes
  module Types
    # map<u> : () {t -> u} -> Array<u>
    # each_with_index<u> : () {(t, Fixnum) -> u } -> Enumerable<t>
    # TODO : @parameters should be of type Arguments
    #
    class Block
      def initialize(parameters, return_type)
        @parameters, @return_type = parameters, return_type
      end

      def inspect
        %Q{{#{@parameters.inspect} -> #{@return_type.inspect}} (Block)}
      end

      def signature_inspect
        %Q{{#{@parameters.signature_inspect} -> #{@return_type.signature_inspect}}}
      end

      alias :to_s :inspect
    end
  end
end

