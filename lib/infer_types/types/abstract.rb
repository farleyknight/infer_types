module InferTypes
  module Types

    # TODO: Keep track of all abstract types, but bound to a specific
    # class.
    #
    # Ex: If we have Array<t> then we can't assume that the abstract
    # type 't' is the same as the 't' in some user class Things<t>
    #
    #
    class Abstract
      @@abstract_types = {}

      # TODO: Generate our own letters based on the information above
      attr_accessor :ident
      class << self
        def from_ident(ident)
          unless ident.is_a? String
            raise "ident must be a string!"
          else
            abstract = self.allocate
            abstract.ident = ident
            @@abstract_types[ident] ||= abstract
          end
        end

        alias :from_id :from_ident

        def new(*args)
          raise "Cannot use Abstract.new!"
        end
      end

      def inspect
        "#{@ident} (Abstract)"
      end

      def signature_inspect
        @ident
      end

      alias :to_s :inspect
    end
  end
end
