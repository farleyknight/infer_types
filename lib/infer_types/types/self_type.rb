require 'set'

module InferTypes::Types
  class SelfType
    # @accessor [rw] [Class]
    attr_accessor :klass

    # @accessor [rw] [Array<Module>]
    attr_accessor :modules

    alias :included_modules :modules

    attr_accessor :superclass

    # @param [Hash]
    # @option [Class] params class
    # @option Array<Module>] params modules
    def initialize(params = {})
      @klass      = params[:class]
      @superclass = params[:superclass]
      @modules    = params[:modules].to_set
    end

    # @param [SelfType] other
    # @return [Boolean]
    def ==(other)
      @klass == other.class and @modules == other.modules
    end

    # @return [String]
    def inspect
      "<self-type class=#{@klass} modules=#{included_modules.inspect}>"
    end

    alias :to_s :inspect
  end
end
