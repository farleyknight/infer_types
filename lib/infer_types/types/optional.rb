module InferTypes
  module Types
    class Optional
      def initialize(type)
        raise "Must be of one of the schemas #{Type.schemas}!" unless InferTypes::Types.schemas.include? type.class
        @type = type
      end

      def inspect
        %Q{?#{@type.inspect} (Optional)}
      end

      def signature_inspect
        %Q{?#{@type.signature_inspect}}
      end

      alias :to_s :inspect
    end
  end
end
