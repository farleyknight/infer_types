# Please check out this file for more definitions!
#
#   vendor/diamondback-ruby-0.20090726/stubs/1.8/base_types.rb

# @todo (farleyknight@gmail.com) Represent this as a union of FalseClass & TrueClass
class Boolean; end

class ::Object
  def_method :== do |m|
    m.param :arg => [Object]
    m.return [Boolean]
  end

  def_method :=== do |m|
    m.param :arg => [Object]
    m.return [Boolean]
  end

  def_method :to_s do |m|
    m.return [String]
  end
end

module ::Kernel
  def_method :puts do |m|
    m.param :arg => [String]
    m.return [NilClass]
  end

  # print?

  # ??
end
