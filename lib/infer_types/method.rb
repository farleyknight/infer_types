require 'lib/infer_types/method/bindings'
require 'lib/infer_types/method/equations'
require 'lib/infer_types/method/environment'
require 'lib/infer_types/method/signature'

module InferTypes
  class Method
    class Definer
      # @param [String, Symbol] name
      # @param [Class, Module] owner
      def initialize(name, owner)
        @name   = name
        @params = []
        @owner  = owner

        if @owner.is_a? Class
          @method_type = :class
        elsif @owner.is_a? Module
          @method_type = :module
        else
          raise "Cannot handle @owner = #{owner}!"
        end
      end

      # @param [Hash] hash
      # @return [Array<Hash>]
      def param(hash)
        @params << hash
      end

      # @param [Array<Type>] type
      # @return [Array<Type>]
      def return(type)
        @return = type
      end

      # @return [Array<Type>]
      def return_type
        @return
      end
    end

    # Allow classes & modules to have the method defined on them after they've
    # already been loaded. These are primarily used on classes & modules that
    # are a part of the standard library.
    #
    #
    module Helper
      def def_method(name)
        # Find a better way to handle this than instance methods, since
        # we don't want to pollute the Class & Module namespace too much.
        @definitions ||= {}

        definer = Definer.new(name, self)
        yield(definer)
        @definitions[name.to_sym] = definer
      end

      def def_methods
        @definitions || {}
      end

      def find_method(name)
        def_methods[name.to_sym]
      end

      alias :lookup_method :find_method
    end

    # @attribute [rw] [Method::Environment]
    attr_accessor :env

    # @attribute [rw] [Class]
    attr_accessor :klass

    # @param [SymExpr] method_body
    # @param [Class] klass
    # @param [String, Symbol] method_name
    def initialize(method_body, klass, method_name)
      @klass = klass
      @env   = Method::Environment.new(method_body, klass, method_name, self)
    end

    # @return [??]
    def body
      @env.method_body
    end

    # @return [??]
    def last_expr
      body.last
    end

    # Determine the return type for this method, using a back tracking
    # search algorithm.
    #
    # @todo (farleyknight@gmail.com): Write up a Type class.
    #
    # @return [Array<Class,Type>]
    def return_type
      infer_for_id(@env.bindings[last_expr])
    end

    def infer_for_id(id)
      @env.equations.solve_for(id)
    end

    # @return [SelfType]
    def self_type
      InferTypes::Types::SelfType.new({
          :class      => @klass,
          :superclass => @klass.superclass,
          :modules    => @klass.included_modules
        })
    end
  end
end

Class.send  :include, InferTypes::Method::Helper
Module.send :include, InferTypes::Method::Helper
