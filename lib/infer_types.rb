require 'rubygems'

if RUBY_VERSION =~ /1\.8/
  require 'parse_tree'
elsif RUBY_VERSION =~ /1\.9/
  require 'ripper'
end

require 'lib/infer_types/patterns'
require 'lib/infer_types/sym_expr'
require 'lib/infer_types/method'
require 'lib/infer_types/array_ext'
require 'lib/infer_types/types'

module InferTypes
  module Helper
    def body
      Patterns[:class].match(to_ast)["body"]
    end

    # @param [String, Symbol] name
    # @return [??]
    def method_body(name)
      body.box.detect do |_def|
        Patterns[_def.first].match(_def)["name"].to_s == name.to_s
      end
    end

    # @param [Symbol] method_name
    # @return [Method]
    def method_for(method_name)
      method_info = self.method_body(method_name)
      if method_info.nil?
        raise "Could not find method named #{method_name}!"
      else
        Method.new(method_info, self, method_name)
      end
    end

    # @todo Check for Ruby version and use different gems based on that..
    #
    # @return [SymExpr]
    def to_ast
      SymExpr.new(ParseTree.translate(self))
    end
  end
end

Class.send  :include, InferTypes::Helper
Module.send :include, InferTypes::Helper
