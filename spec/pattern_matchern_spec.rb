require 'spec_helper'

require "infer_types"

describe InferTypes::Patterns::Class do
  class A; end

  let (:a) {
    Class.new do
      def m
        'a'
      end
    end
  }

  let (:b) {
    Class.new do
      def m
        'a'
      end

      def n
        3
      end
    end
  }

  it "should correctly match the name on the class A" do
    match = InferTypes::Patterns::Class.match(A.to_ast)
    match["class"].should == :A
  end

  it "should correctly match on `a` class" do
    match = InferTypes::Patterns::Class.match(a.to_ast)
    match["superclass"].should == :Object
    match["body"].to_a.should == [:defn, :m, [:scope, [:block, [:args], [:str, "a"]]]]
  end

  it "should correctly match on `b` class" do
    match = InferTypes::Patterns::Class.match(b.to_ast)
    match["superclass"].should == :Object
    match["body"].to_a.should == [[:defn, :m, [:scope, [:block, [:args], [:str, "a"]]]], [:defn, :n, [:scope, [:block, [:args], [:lit, 3]]]]]
  end
end

