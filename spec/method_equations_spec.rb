require 'spec_helper'
require 'infer_types'

describe InferTypes::Method::Equations do
  let (:a) {
    Class.new do
      def m
        s = 500
        t = s
        t
      end
    end
  }

  #   puts m.bindings.inspect =>
  #
  #   1 := (lasgn s (lit 500))
  #   2 := (lit 500)
  #   3 := (lasgn t (lvar s))
  #   4 := (lvar s)
  #   5 := (lvar t)

  #   puts m.equations.inspect =>
  #
  #   #1 => #2
  #   s  => #2
  #   #2 => Fixnum
  #   #3 => s
  #   t  => #4
  #   #4 => s
  #   #5 => t
  #

  it "should produce the correct equations" do
    a.method_for(:m).env.equations.should == [
      equation(1,  2),
      equation(:s, 2),
      equation(2,  Fixnum),
      equation(3,  4),
      equation(:t, 4),
      equation(4,  :s),
      equation(5,  :t)
    ]
  end

  it "should determine that the expr tagged 5 should be a Fixnum" do
    a.method_for(:m).env.equations.solve_for(5).should == [Fixnum]
  end

  let (:b) {
    Class.new do
      def m
        "hey"
      end
    end
  }

  it "should determine that the expr tagged ? should be a String" do
    b.method_for(:m).env.equations.should == [equation(1, String)]
    b.method_for(:m).env.equations.solve_for(1).should == [String]
  end

  let (:c) {
    Class.new do
      def p
        puts 'whatever'
      end
    end
  }

  pending "Test for the self-type in this set of equations" do
    c.method_for(:p).env.equations.should_not == []
  end

  def equation(source, target)
    InferTypes::Method::Equation.new(:source => source, :target => target)
  end
end
