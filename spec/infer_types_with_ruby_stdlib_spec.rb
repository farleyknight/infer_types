require 'spec_helper'

describe "" do
  let (:a) {
    Class.new do
      def m
        1 == 2
      end

      def n
        1 === 3
      end

      def p
        puts 'whatever'
      end
    end
  }

  it "should infer the return type of a method using Kernel#puts" do
    # puts a.method_for(:p).body.to_a.inspect
    # puts a.method_for(:p).env.equations.inspect
    a.method_for(:p).return_type.should == [NilClass]
  end

  it "should infer the return type of a method using Object#==" do
    # puts a.method_for(:m).body.to_a.inspect
    # puts a.method_for(:m).env.equations.inspect
    a.method_for(:m).return_type.should == [Boolean]
  end

  it "should infer the return type of a method using Object#===" do
    # puts a.method_for(:n).body.to_a.inspect
    # puts a.method_for(:n).env.equations.inspect
    a.method_for(:n).return_type.should == [Boolean]
  end
end
