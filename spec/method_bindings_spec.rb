require 'spec_helper'
require 'infer_types'

describe InferTypes::Method::Bindings do
  let(:a) {
    Class.new do
      def m
        s = 500
        t = s
        t
      end
    end
  }

  def binding(id, expr)
    InferTypes::Method::Binding.new(:key => id, :expr => expr.to_symexpr)
  end

  #   puts m.bindings.inspect =>
  #
  #   1 := (lasgn s (lit 500))
  #   2 := (lit 500)
  #   3 := (lasgn t (lvar s))
  #   4 := (lvar s)
  #   5 := (lvar t)

  it "should produce the correct bindings for objects" do
    a.method_for(:m).env.bindings.should == [
      binding(1, [:lasgn, :s, [:lit, 500]]),
      binding(2, [:lit, 500]),
      binding(3, [:lasgn, :t, [:lvar, :s]]),
      binding(4, [:lvar, :s]),
      binding(5, [:lvar, :t])
    ]
  end

  let(:b) {
    Class.new do
      def self.m
        100
      end
    end
  }

  it "should correctly handle self methods" do
    b.method_for(:m).env.bindings.should == []
  end

  it "" do
    b.self_method_for(:m).env.bindings.should == []
  end
end
