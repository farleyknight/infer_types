require 'spec_helper'

describe "Inferring types" do
  let(:a) {
    Class.new {
      def string
        'a'
      end

      def fixnum
        3
      end

      def regexp
        /what/
      end

      def _nil
        nil
      end

      def _true
        true
      end

      def _false
        false
      end

      def m
        s = 1
        t = '2'
        s
      end

      def _if
        if true
          1
        else
          '3'
        end
      end

      def _unless
        unless true
          1
        else
          '3'
        end
      end

      def p
        puts 'whatever'
      end

      def s
        to_s
      end
    }
  }

  it "should infer the return type of a method which returns a constant string" do
    a.method_for(:string).return_type.should == [String]
  end

  it "should infer the return type of a method which returns a constant fixnum" do
    a.method_for(:fixnum).return_type.should == [Fixnum]
  end

  it "should infer the return type of a method which returns a constant regexp" do
    a.method_for(:regexp).return_type.should == [Regexp]
  end

  it "should infer the return type of a method which returns nothing" do
    a.method_for(:_nil).return_type.should == [NilClass]
  end

  it "should infer the return type of a method which returns true" do
    a.method_for(:_true).return_type.should == [TrueClass]
  end

  it "should infer the return type of a method which returns false" do
    a.method_for(:_false).return_type.should == [FalseClass]
  end

  it "should infer the return type of `m` as being the same as the variable `s`" do
    a.method_for(:m).return_type.should == [Fixnum]
  end

  it "should infer the return type of `_if` as being both a Fixnum or a String" do
    a.method_for(:_if).return_type.to_set.should == [Fixnum, String].to_set
  end

  it "should infer the return type of `_unless` as being both a Fixnum or a String" do
    a.method_for(:_unless).return_type.to_set.should == [Fixnum, String].to_set
  end

  it "should infer the return type of `p` as being nil" do
    a.method_for(:p).return_type.should == [NilClass]
  end

  it "should infer the return type of `s` as being String" do
    a.method_for(:s).return_type.should == [String]
  end
end
